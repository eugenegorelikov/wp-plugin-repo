<?php

/**
 * Class TOOBase
 */
class TOOBase
{

    public $api_version = 1;

    /**
     * @param $post_ID
     * @param $metaname
     * @param $too_object
     *
     * @return bool
     */
    protected function update_meta( $post_ID, $metaname, $too_object )
    {
        $meta = get_post_meta($post_ID, $metaname, true);
        if ( empty($too_object) && ! empty($meta) ) {
            delete_post_meta($post_ID, $metaname);
        } else if ( empty($too_object) ) {
            return true;
        }
        if ( $meta == $too_object ) {
            return true;
        } else if ( ! empty($meta) ) {
            update_post_meta($post_ID, $metaname, $too_object, $meta);
        } else {
            $add = add_post_meta($post_ID, $metaname, $too_object, true);
            //If value was empty, but meta existed
            if ( ! $add ) {
                update_post_meta($post_ID, $metaname, $too_object, $meta);
            }
        }

        return true;
    }

    /**
     * @param string $endpoint
     * @param bool   $id
     *
     * @return array|bool|mixed|null|object
     */
    protected function getAPIResponse( $endpoint = 'vacancy', $id = false )
    {
        $url = $this->get_url() . "/v" . $this->api_version . "/" . $endpoint;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'X-Channel: ' . get_option('options_too_channel_token')
        ]);

        $response = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ( $status == 200 ) {
            $response = json_decode($response, true);
        } else {
            $response = false;
        }

        curl_close($ch);

        return $response;
    }

    public static function too_remote_get( $endpoint = 'vacancy' )
    {
        $url = 'http://www.jobsrepublictoo.nl/public-api/v1/'.$endpoint;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'X-Channel: ' . get_option('options_too_channel_token')
        ]);

        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ( $status == 200 ) {
            $response = json_decode($response, true);
        } else {
            $response = false;
        }

        curl_close($ch);

        return $response;
    }

    /**
     * @return string
     */
    private function get_url()
    {
        // if ( defined('TOO_SERVER') ) {
        //     switch ( constant('TOO_SERVER') ) {
        //         // case "ACC":
        //             //acceptance
        //             // return 'http://acc.jobsrepublictoo.nl/public-api';
        //         case "TEST":
        //             //testing
        //             return 'http://test.jobsrepublictoo.nl/public-api';
        //         default:
        //             //production
        //             return 'http://www.jobsrepublictoo.nl/public-api';
        //     }
        // }

        return 'http://www.jobsrepublictoo.nl/public-api';
    }

    /**
     * @param string $endpoint
     * @param        $data
     *
     * @return array|bool|mixed|null|object
     */
    protected function getAPIPost( $endpoint = 'vacancy', $data )
    {
        $url = $this->get_url() . "/v" . $this->api_version . "/" . $endpoint;

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [            
            'Content-Type: application/json',
            'X-Channel: ' . get_option('options_too_channel_token')
        ]);

        $response = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ( $status == 200 || $status == 201 || $status == 204 ) {
            $response = json_decode($response, true);
        } else {
            $response = false;
        }

        curl_close($ch);

        return $response;
    }

    /**
     * @param string $endpoint
     * @param        $data
     *
     * @return array|bool|mixed|null|object
     */
    public function getAPIPostTest( $endpoint = 'vacancy', $data )
    {
        $url = "http://test.jobsrepublictoo.nl/public-api/v" . $this->api_version . "/" . $endpoint;

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [            
            'Content-Type: application/json',
            'X-Channel: 3fd9cdab92e58341ca0c117a5aa39b75'
        ]);

        $response = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ( $status == 200 || $status == 201 || $status == 204 ) {
            $response = json_decode($response, true);
        } else {
            $response = false;
        }

        curl_close($ch);

        return $response;
    }

    /**
     * @param $timestamp
     *
     * @return false|int
     */
    protected function timestamp_to_gmt( $timestamp )
    {
        $second = gmdate('s', $timestamp);
        $minute = gmdate('i', $timestamp);
        $hour = gmdate('H', $timestamp);
        $day = gmdate('d', $timestamp);
        $month = gmdate('m', $timestamp);
        $year = gmdate('Y', $timestamp);

        return mktime($hour, $minute, $second, $month, $day, $year);
    }
}
