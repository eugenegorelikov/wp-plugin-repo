<?php

/**
 * Class TOO
 */
class TOO extends TOOBase
{

    /**
     * TOO constructor.
     */
    public function __construct()
    {
        global $application;

        $vacancy     = new Vacancy();
        $company     = new Company();
        $application = new Application();

        //on init, create the post types
        add_action('init', [ $vacancy, 'init' ]);
        add_action('init', [ $company, 'init' ]);
        add_action('init', [ $application, 'init' ]);

        //retrieve vacancies from ONE
        add_action('pre_get_posts', [ $vacancy, 'pre_get_posts' ]);

        //log count to ONE
        add_action('wp', [ $vacancy, 'log_view' ]);
        add_action('wp_footer', [ $vacancy, 'log_javascript' ]);

        //add pages
        add_filter('template_include', [ $application, 'template_include' ]);

        //auto update core
        add_filter('automatic_updates_is_vcs_checkout', '__return_false', 1);
        add_filter('allow_major_auto_core_updates', '__return_true');
    }
}

$too_channel_token = get_option('options_too_channel_token');
if ( ! empty($too_channel_token) ) {
    $too = new TOO();
}

/**
 * Get the total count of main query
 *
 * @since 1.0.0
 *
 * @return int Count of rows
 */
function get_the_query_count()
{
    global $wp_query;

    return $wp_query->post_count;
}

/**
 * Output the total count of main query
 *
 * @since 1.0.0
 */
function the_query_count()
{
    echo get_the_query_count();
}
