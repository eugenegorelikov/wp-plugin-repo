<?php

/**
 * Class Application
 */
class Application extends TOOBase
{

    //TOO Object is arguments in Wordpress
    private $too_object = null;
    private $files = null;
    //Array with errors
    private $errors = [];

    /**
     * Application constructor.
     *
     * @param array $too_object
     * @param array $files
     */
    public function __construct($too_object = [], $files = [])
    {
        $this->too_object = $too_object;
        $this->files = $files;
    }

    public function init()
    {
        add_rewrite_endpoint('solliciteren', EP_PERMALINK | EP_PAGES);
    }

    /**
     * @param $template
     */
    public function template_include($template)
    {
        global $wp_query;
        global $can_apply;

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && get_post_type() == 'vacancy' && isset($wp_query->query_vars['solliciteren'])) {
            $this->too_object = $_POST;
            $this->files = $_FILES;
            if ($this->save()) {
                return get_template_part('apply', 'success');
            }
        }

        if (get_post_type() == 'vacancy' && isset($wp_query->query_vars['solliciteren'])) {
            if (! $can_apply) {
//                return get_template_part('apply', 'error');
                return get_template_part('apply', 'api');
            }

            return get_template_part('apply', 'api');
        }

        return $template;
    }

    /**
     * Send application to REST server and return result
     *
     * @return bool
     */
    public function save()
    {
        $format = $this->_validate();
        if ($format == false) {
            return false;
        }

        $response = $this->getAPIPost(
            'vacancy/' . get_post_meta(get_the_ID(), 'tooID', true) . '/application',
            $format
        );

        //is output ok?
        if ($response !== false) {
            $page = get_page_by_path('bedankt');
            if ($page->ID) {
                header('Location: ' . get_page_link($page));
                die();
            }

            return true;
        } else {
            if (is_array($response)) {
                $this->errors = $response;
            } else {
                $this->errors = [ 'Er is een fout opgetreden bij het versturen van de sollicitatie. Neem contact met ons op als deze foutmelding vaker wordt weergegeven.' ];
            }

            return false;
        }
    }

    /**
     * @return bool|mixed|string|void
     */
    private function _validate()
    {
        //check if data needs to be manipulated to improve easy form

        //validate if all required data is there
        if (! (get_post_meta(get_the_ID(), 'tooID', true) > 0)) {
            $this->errors[] = "Vacature is onbekend";
        }
        if (empty($this->too_object['firstname'])) {
            $this->errors[] = "Voornaam is een verplicht veld";
        }
        if (empty($this->too_object['lastname'])) {
            $this->errors[] = "Achternaam is een verplicht veld";
        }
        if (empty($this->too_object['email'])) {
            $this->errors[] = "E-mail is een verplicht veld";
        }
        if (empty($this->too_object['telephone'])) {
            $this->errors[] = "Telefoonnummer is een verplicht veld";
        }
        if (! isset($this->files['cv']) || ! is_uploaded_file($this->files['cv']['tmp_name'])) {
            $this->errors[] = "CV is een verplicht veld";
        }

        if (! empty($this->errors)) {
            return false;
        }

        $format = [
            'applicant' => [
                'firstname' => $this->too_object['firstname'],
                'lastname'  => $this->too_object['lastname'],
                'phone'     => $this->too_object['telephone'],
                'email'     => $this->too_object['email'],
            ],
            'cv_file'   => [
                'file_name'    => $this->files['cv']['name'],
                'file_content' => base64_encode(file_get_contents($this->files['cv']['tmp_name']))
            ]
        ];

        //add optional fields
        if (isset($this->files['motivationFile']) && is_uploaded_file($this->files['motivationFile']['tmp_name'])) {
            $format['motivation_file'] = [
                'file_name'    => $this->files['motivationFile']['name'],
                'file_content' => base64_encode(file_get_contents($this->files['motivationFile']['tmp_name']))
            ];
        }

        if (! empty($this->too_object['motivation'])) {
            $format['motivation'] = $this->too_object['motivation'];
        }

        return json_encode($format);
    }

    /**
     * Validate if all required data is present
     *
     * @return array
     */
    public function get_errors()
    {
        return $this->errors;
    }
}

function is_vacatures_one_url($url)
{
    return
        strpos($url, 'https://vacatures.one/') === 0 ||
        strpos($url, 'http://vacatures.one/') === 0 ||
        strpos($url, 'https://test.vacatures.one/') === 0 ||
        strpos($url, 'http://test.vacatures.one/') === 0;
}

function get_external_link()
{
    $external_link = get_post_meta(
        get_the_ID(),
        'application_url',
        true
    );

    // Remove quotes
    $external_link = str_replace('"', '', $external_link);
    return $external_link;
}

/**
 * Output the url to link to the application page
 *
 * @since 1.0.0
 */
function get_applicationlink()
{
    $external_link = get_external_link();

    $link = empty($external_link) || is_vacatures_one_url($external_link) ? trailingslashit(get_permalink()) . 'solliciteren' : $external_link;

    return $link;
}

function has_local_applicationlink()
{
    $external_link = get_external_link();
    return is_vacatures_one_url($external_link);
}

/**
 * The url to link to the application page
 *
 * @since 1.0.0
 *
 * @return string Url of application page
 */
function the_applicationlink()
{
    echo get_applicationlink();
}


function get_too_id()
{
    return get_post_meta(get_the_ID(), 'tooID', true);
}

/**
 * If the provided $url is an external link, it will
 * echo out target="_blank".
 *
 * @param  string $url A valid URL
 * @return string      null | ' target="_blank"'
 */
function the_target($url)
{
    if (empty($url)) {
        echo '';
    }

    $link_url = parse_url($url);
    $home_url = parse_url(home_url());

    if (array_key_exists('host', $link_url) && array_key_exists('host', $home_url)) {
        if ($link_url['host'] !== $home_url['host']) {
            echo 'target="_blank"';
        }
    }

    echo '';
}

/**
 * Get validation errors after application form submit
 *
 * @since 1.0.0
 *
 * @return array List of all errors
 */
function get_application_errors()
{
    global $application;

    $html = '';
    if (count($application->get_errors()) > 0) {
        $html = "<ul>";
        foreach ($application->get_errors() as $error) {
            $html .= "<li>" . $error . "</li>";
        }
        $html .= "</ul>";
    }

    return $html;
}

function the_application_errors()
{
    echo get_application_errors();
}

/**
 * @param      $id
 * @param      $name
 * @param      $label
 * @param bool $required
 * @param bool $autofocus
 *
 * @return string
 */
function get_input_field($id, $name, $label, $required = false, $autofocus = false)
{
    $input = '';
    if (! empty($label)) {
        $input .= '<label for="' . $id . '">' . $label;
        if ($required) {
            $input .= ' <span class="required">*</span>';
        }
        $input .= '</label>';
    }
    $input .= '<input type="text" name="' . $name . '" class="form-control"';
    if (isset($_POST[$name])) {
        $input .= ' value="' . $_POST[$name] . '"';
    }
    $input .= ' id="' . $id . '"';
    if ($required) {
        $input .= ' required';
    }
    if ($autofocus) {
        $input .= ' autofocus';
    }
    $input .= ' />';

    return $input;
}

/**
 * @param      $id
 * @param      $name
 * @param      $label
 * @param bool $required
 * @param bool $autofocus
 *
 * @return string
 */
function get_file_field($id, $name, $label, $required = false, $autofocus = false)
{
    $input = '';
    if (! empty($label)) {
        $input .= '<label for="' . $id . '">' . $label;
        if ($required) {
            $input .= ' <span class="required">*</span>';
        }
    }
    $input .= '<input type="file" name="' . $name . '" id="' . $id . '"';
    if ($required) {
        $input .= ' required';
    }
    if ($autofocus) {
        $input .= ' autofocus';
    }
    $input .= ' />';
    if (! empty($label)) {
        $input .= '</label>';
    }

    return $input;
}

/**
 * Get the birthday input elements
 *
 *
 * @since 1.0.0
 *
 * @param string $label String to display in label field
 *
 * @return string HTML for birthday input
 */
function get_the_birthday_field($label = 'Geboortedatum')
{
    $input = '<label for="birthdayDay">' . $label . '</label><input type="text" name="birthdayDay" maxlength="2"';
    if (isset($_POST['birthdayDay'])) {
        $input .= ' value="' . $_POST['birthdayDay'] . '"';
    }
    $input .= ' class="form-control dateDay" id="birthdayDay" />&nbsp;<input type="text" class="form-control dateMonth" name="birthdayMonth" maxlength="2"';
    if (isset($_POST['birthdayMonth'])) {
        $input .= ' value="' . $_POST['birthdayMonth'] . '"';
    }
    $input .= ' id="birthdayMonth" />&nbsp;<input type="text" name="birthdayYear" maxlength="4"';
    if (isset($_POST['birthdayYear'])) {
        $input .= ' value="' . $_POST['birthdayYear'] . '"';
    }
    $input .= ' class="form-control dateYear" id="birthdayYear" />';

    return $input;
}

/**
 * @param string $label
 */
function the_birthday_field($label = 'Geboortedatum')
{
    echo get_the_birthday_field($label);
}

/**
 * @return string
 */
function get_the_birthday_day_field()
{
    $input = '<input type="text" name="birthdayDay" maxlength="2"';
    if (isset($_POST['birthdayDay'])) {
        $input .= ' value="' . $_POST['birthdayDay'] . '"';
    }
    $input .= ' class="form-control dateDay" id="birthdayDay" />';

    return $input;
}

function the_birthday_day_field()
{
    echo get_the_birthday_day_field();
}

/**
 * @return string
 */
function get_the_birthday_month_field()
{
    $input = '<input type="text" class="form-control dateMonth" name="birthdayMonth" maxlength="2"';
    if (isset($_POST['birthdayMonth'])) {
        $input .= ' value="' . $_POST['birthdayMonth'] . '"';
    }
    $input .= ' id="birthdayMonth" />';

    return $input;
}

/**
 * @param string $label
 */
function the_birthday_month_field($label = 'Geboortedatum')
{
    echo get_the_birthday_month_field();
}

/**
 * @return string
 */
function get_the_birthday_year_field()
{
    $input = '<input type="text" name="birthdayYear" maxlength="4"';
    if (isset($_POST['birthdayYear'])) {
        $input .= ' value="' . $_POST['birthdayYear'] . '"';
    }
    $input .= ' class="form-control dateYear" id="birthdayYear" />';

    return $input;
}

function the_birthday_year_field()
{
    echo get_the_birthday_year_field();
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_gender_field($label = 'Geslacht')
{
    $input = '<label>' . $label . '</label><label for="genderMale" class="radio-inline"><input type="radio" name="gender" value="M" id="genderMale"';
    if (isset($_POST['gender']) && $_POST['gender'] == "M") {
        $input .= ' checked';
    }
    $input .= ' /> Man</label><label for="genderFemale" class="radio-inline"><input type="radio" name="gender" value="V" id="genderFemale"';
    if (isset($_POST['gender']) && $_POST['gender'] == "V") {
        $input .= ' checked';
    }
    $input .= ' /> Vrouw</label>';

    return $input;
}

/**
 * @param string $label
 */
function the_gender_field($label = 'Geslacht')
{
    echo get_the_gender_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_motivation_field($label = 'Of type uw motivatie')
{
    $input = '';
    if (! empty($label)) {
        $input .= '<label for="motivation">' . $label . '</label>';
    }
    $input .= '<textarea name="motivation" id="motivation" class="form-control">';
    if (isset($_POST['motivation'])) {
        $input .= $_POST['motivation'];
    }
    $input .= '</textarea>';

    return $input;
}

/**
 * @param string $label
 */
function the_motivation_field($label = 'Of type uw motivatie')
{
    echo get_the_motivation_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_submit_button($label = 'Sollicitatie versturen')
{
    return '<button id="submit" type="submit" class="submit btn btn-primary btn-lg" name="submit">' . $label . '</button>';
}

/**
 * @param string $label
 */
function the_submit_button($label = 'Sollicitatie versturen')
{
    echo get_the_submit_button($label);
}

/**
 * @param string $label
 * @param bool   $autofocus
 *
 * @return string
 */
function get_the_firstname_field($label = 'Voornaam', $autofocus = true)
{
    return get_input_field('firstname', 'firstname', $label, true, $autofocus);
}

/**
 * @param string $label
 * @param bool   $autofocus
 */
function the_firstname_field($label = 'Voornaam', $autofocus = true)
{
    echo get_the_firstname_field($label, $autofocus);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_prefix_field($label = 'Tussenvoegsels')
{
    return get_input_field('prefix', 'prefix', $label);
}

/**
 * @param string $label
 */
function the_prefix_field($label = 'Tussenvoegsels')
{
    echo get_the_prefix_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_lastname_field($label = 'Achternaam')
{
    return get_input_field('lastname', 'lastname', $label, true);
}

/**
 * @param string $label
 */
function the_lastname_field($label = 'Achternaam')
{
    echo get_the_lastname_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_nationality_field($label = 'Nationaliteit')
{
    return get_input_field('nationality', 'nationality', $label);
}

/**
 * @param string $label
 */
function the_nationality_field($label = 'Nationaliteit')
{
    echo get_the_nationality_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_address_field($label = 'Adres')
{
    return get_input_field('address', 'address', $label);
}

/**
 * @param string $label
 */
function the_address_field($label = 'Adres')
{
    echo get_the_address_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_zipcode_field($label = 'Postcode')
{
    return get_input_field('zipcode', 'zipcode', $label);
}

/**
 * @param string $label
 */
function the_zipcode_field($label = 'Postcode')
{
    echo get_the_zipcode_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_city_field($label = 'Woonplaats')
{
    return get_input_field('city', 'city', $label);
}

/**
 * @param string $label
 */
function the_city_field($label = 'Woonplaats')
{
    echo get_the_city_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_telephone_field($label = 'Telefoon')
{
    return get_input_field('telephone', 'telephone', $label, true);
}

/**
 * @param string $label
 */
function the_telephone_field($label = 'Telefoon')
{
    echo get_the_telephone_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_mobile_field($label = 'Mobiel')
{
    return get_input_field('mobile', 'mobile', $label);
}

/**
 * @param string $label
 */
function the_mobile_field($label = 'Mobiel')
{
    echo get_the_mobile_field($label);
}

/**
 * @param string $label
 * @param bool   $autofocus
 *
 * @return string
 */
function get_the_email_field($label = 'E-mailadres', $autofocus = false)
{
    return get_input_field('email', 'email', $label, true, $autofocus);
}

/**
 * @param string $label
 * @param bool   $autofocus
 */
function the_email_field($label = 'E-mailadres', $autofocus = false)
{
    echo get_the_email_field($label, $autofocus);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_motivationfile_field($label = 'Upload uw sollicitatiebrief')
{
    return get_file_field('motivationFile', 'motivationFile', $label);
}

/**
 * @param string $label
 */
function the_motivationfile_field($label = 'Upload uw sollicitatiebrief')
{
    echo get_the_motivationfile_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_cv_field($label = 'Upload uw CV')
{
    return get_file_field('cv', 'cv', $label, true);
}

/**
 * @param string $label
 */
function the_cv_field($label = 'Upload uw CV')
{
    echo get_the_cv_field($label);
}

/**
 * @param string $label
 *
 * @return string
 */
function get_the_picture_field($label = 'Upload uw foto')
{
    return get_file_field('picture', 'picture', $label);
}

/**
 * @param string $label
 */
function the_picture_field($label = 'Upload uw foto')
{
    echo get_the_picture_field($label);
}
