<?php

/**
 * Class Company
 */
class Company extends TOOBase
{

    private $too_id = null;
    private $too_object = null;

    /**
     * Company constructor.
     *
     * @param null $too_id
     */
    public function __construct( $too_id = null )
    {
        $this->too_id = $too_id;
    }

    public function init()
    {
        //post type for company info
        if ( get_theme_support('post-formats') !== false ) {
            register_post_type('company',
                [
                    'labels'            => [
                        'name'          => __('Organisatie'),
                        'singular_name' => __('Organisatie')
                    ],
                    'supports'          => [ 'title', 'editor', 'custom-fields', 'excerpt', 'author', 'thumbnail' ],
                    'public'            => true,
                    'rewrite'           => [ 'slug' => 'organisaties' ],
                    'has_archive'       => true,
                    'show_in_admin_bar' => false,
                    'show_in_menu'      => false,
                    'hierarchical'      => false
                ]
            );
            // flush_rewrite_rules(); // This is realy bad for performance!
        }
    }

    /**
     * @return false|int|WP_Error
     */
    public function save()
    {
        $this->too_object = $this->getAPIResponse('company/' . $this->too_id);

        $post_ID = 0;
        $query = new WP_Query([
            'numberposts' => -1,
            'post_type'   => 'company',
            'meta_key'    => 'tooID',
            'meta_value'  => $this->too_object['id']
        ]);
        if ( $query->have_posts() ) {
            //we got it, update!
            $i = 0;
            while ( $query->have_posts() ) {
                $query->the_post();
                //update the existing post
                if ( $i == 0 ) {
                    $post_ID = get_the_ID();
                    $this->update($post_ID);
                } else {
                    wp_delete_post(get_the_ID(), true);
                } // sometimes companies are twice in database
                $i++;
            }
        } else {
            //insert new post
            $post_ID = $this->insert();
        }

        //save media of the company
        if ( isset($this->too_object['logo']) &&
            isset($this->too_object['logo']['file_name']) &&
            isset($this->too_object['logo']['file_content']) &&
            ! empty($this->too_object['logo']['file_name']) &&
            ! empty($this->too_object['logo']['file_content']) &&
            ( ! has_post_thumbnail($post_ID) || filemtime(get_attached_file(get_post_thumbnail_id())) < date("U") - 86400 ) ) {
            $file = wp_upload_bits($this->too_object['logo']['file_name'], null,
                base64_decode($this->too_object['logo']['file_content']));

            // Prepare an array of post data for the attachment.
            $attachment = [
                'guid'           => $file['url'],
                'post_mime_type' => $file['type'],
                'post_title'     => preg_replace('/\.[^.]+$/', '', basename($file['file'])),
                'post_content'   => '',
                'post_status'    => 'inherit'
            ];

            // Insert the attachment.
            $attach_id = wp_insert_attachment($attachment, $file['file'], $post_ID);

            // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata($attach_id, $file['file']);
            wp_update_attachment_metadata($attach_id, $attach_data);

            set_post_thumbnail($post_ID, $attach_id);
        }

        return $post_ID;
    }

    //insert new post

    /**
     * @return int|WP_Error
     */
    public function insert()
    {
        $wp_error = false;
        $post_ID = wp_insert_post([
            'post_type'      => 'company',
            'post_title'     => wp_strip_all_tags($this->too_object['name']),
            'post_content'   => $this->too_object['description'],
            'post_status'    => 'publish',
            'post_author'    => 1, // TODO: author ?
            'comment_status' => 'closed',
        ], $wp_error);

        if ( ! $wp_error ) {
            $this->update_meta($post_ID, 'tooID', $this->too_object['id']);
            $this->update_meta($post_ID, 'phone', $this->too_object['phone']);
            $this->update_meta($post_ID, 'short_description', $this->too_object['short_description']);
            $this->update_meta($post_ID, 'address', json_encode($this->too_object['address']));

            return $post_ID;
        }

        return 0;
    }

    //update the post
    //check if anything is changed to improve page load
    /**
     * @param $post_ID
     */
    public function update( $post_ID )
    {
        $update = false;

        if ( $this->too_object['name'] !== get_the_title($post_ID) ) {
            $update = true;
        }

        if ( $this->too_object['phone'] !== get_post_meta($post_ID, 'phone', true) ) {
            $this->update_meta($post_ID, 'phone', $this->too_object['phone']);
        }

        if ( $this->too_object['short_description'] !== get_post_meta($post_ID, 'short_description', true) ) {
            $this->update_meta($post_ID, 'short_description', $this->too_object['short_description']);
        }

        if ( json_encode($this->too_object['address']) !== str_replace("/", "\/",
                get_post_meta($post_ID, 'address', true)) ) {
            $update = $this->update_meta($post_ID, 'address', json_encode($this->too_object['address']));
        }

        if ( $update ) {
            wp_update_post([
                'ID'         => $post_ID,
                'post_title' => wp_strip_all_tags($this->too_object['name'])
            ]);
        }

        return;
    }
}
